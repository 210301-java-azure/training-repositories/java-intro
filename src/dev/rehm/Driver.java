package dev.rehm;

public class Driver {

    public static void main(String[] args){
        System.out.println("Hello World");
        for(int i=0; i<args.length; i++){ // i is the index
            System.out.println(args[i]);
        }

        int[] arrayOne = {8,3,84,92,5};
        int[] arrayTwo = new int[5];
        for(int i : arrayOne) { //i is the number in the array
            System.out.println(i);
        }


//        System.out.println(args[0]);
//        System.out.println(args[1]);
    }


    public void someOtherMethod(){
        int num = 25;
    }
}
