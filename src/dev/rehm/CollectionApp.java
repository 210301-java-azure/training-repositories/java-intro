package dev.rehm;

import dev.rehm.models.StateId;

import java.util.*;

public class CollectionApp {

    public static void main(String[] args) {
        /*
        int x = 5;
        Integer y = new Integer(7);
        y = new Integer(x);

        LinkedList<Object> myList = new LinkedList<>();
        myList.add(9); // autoboxing converts my primitive to its wrapper class automatically
//        LinkedList<int> myList2 = new LinkedList<>();

        HashMap<String, Integer> myMap = new HashMap<>();
        myMap.put("Jane", 200);

        ArrayList list = new ArrayList();
         */

        /* with an integer, natural ordering is already provided
        TreeMap<Integer, Boolean> voterRegistrationById = new TreeMap<>();
        StateId id1 = new StateId(4890234, "John Doe", "35 main str");
        StateId id2 = new StateId(4890235, "Jane Doe", "37 main str");
        voterRegistrationById.put(id1.getIdNo(), false);
        voterRegistrationById.put(id2.getIdNo(), true);
        System.out.println(voterRegistrationById.size());
         */


        TreeMap<StateId, Boolean> voterRegistration = new TreeMap<>();
        StateId id1 = new StateId(4890234, "John Doe", "35 main str");
        StateId id2 = new StateId(4890235, "Jane Doe", "37 main str");
        StateId id3 = new StateId(4890230, "Sally Johnson", "483 chestnut str");
        StateId id4 = new StateId(4890225, "Kate Henry", "8493 1st ave");
        /*
        voterRegistration.put(id1, false);
        voterRegistration.put(id2, true);
        voterRegistration.put(id3, false);
        voterRegistration.put(id4, true);
//        System.out.println(voterRegistration.size());
        for(StateId id: voterRegistration.keySet()){
            System.out.println(id+" --- registered to vote? "+ voterRegistration.get(id));
        }
         */

        HashSet<StateId> idCardSet = new HashSet<>();
        idCardSet.add(id1);
        System.out.println(idCardSet);

        idCardSet.add(id2);
        System.out.println(idCardSet);

        idCardSet.add(id3);
        System.out.println(idCardSet);

        idCardSet.add(id4);
        System.out.println(idCardSet);

        throw new NullPointerException();

    }

}
